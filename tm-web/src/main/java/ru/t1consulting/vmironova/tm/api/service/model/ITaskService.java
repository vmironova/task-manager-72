package ru.t1consulting.vmironova.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.vmironova.tm.enumerated.Status;
import ru.t1consulting.vmironova.tm.model.Task;

import java.util.List;

public interface ITaskService {

    @NotNull
    Task add(@NotNull Task model) throws Exception;

    @NotNull
    Task addByUserId(@Nullable String userId, @NotNull Task model) throws Exception;

    void changeTaskStatusById(
            @Nullable String id,
            @Nullable Status status
    ) throws Exception;

    void changeTaskStatusByUserIdAndId(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    ) throws Exception;

    void clear() throws Exception;

    void clearByUserId(@Nullable String userId) throws Exception;

    int count() throws Exception;

    int countByUserId(@Nullable String userId) throws Exception;

    @NotNull
    Task create(@Nullable String userId, @Nullable String name) throws Exception;

    @NotNull
    Task create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    boolean existsById(@Nullable String id) throws Exception;

    boolean existsByUserIdAndId(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    List<Task> findAll() throws Exception;

    @Nullable
    List<Task> findAllByUserId(@Nullable String userId) throws Exception;

    @Nullable
    List<Task> findAllByProjectId(
            @Nullable String projectId
    ) throws Exception;

    @Nullable
    List<Task> findAllByUserIdAndProjectId(
            @Nullable String userId,
            @Nullable String projectId
    ) throws Exception;

    @Nullable
    Task findOneById(@Nullable String id) throws Exception;

    @Nullable
    Task findOneByUserIdAndId(@Nullable String userId, @Nullable String id) throws Exception;

    void remove(@Nullable Task model) throws Exception;

    void removeByUserId(@Nullable String userId, @Nullable Task model) throws Exception;

    void removeById(@Nullable String id) throws Exception;

    void removeByUserIdAndId(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    Task update(@Nullable Task model) throws Exception;

    @Nullable
    Task updateByUserId(@Nullable String userId, @Nullable Task model) throws Exception;

    @Nullable
    Task updateById(
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    @Nullable
    Task updateByUserIdAndId(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    void updateProjectIdById(
            @Nullable final String id,
            @Nullable final String projectId
    ) throws Exception;

    void updateProjectIdByUserIdAndId(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String projectId
    ) throws Exception;

}
