package ru.t1consulting.vmironova.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.format.annotation.DateTimeFormat;
import ru.t1consulting.vmironova.tm.enumerated.Status;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_project")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "projectDto", propOrder = {
        "id",
        "name",
        "description",
        "status",
        "created",
        "createdBy",
        "updated",
        "updatedBy",
        "userId"
})
@EntityListeners(AuditingEntityListener.class)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class ProjectDTO {

    private static final long serialVersionUID = 1;

    @Id
    @NotNull
    @XmlElement(required = true)
    private String id = UUID.randomUUID().toString();

    @Column
    @NotNull
    @XmlElement(required = true)
    private String name = "";

    @Column
    @NotNull
    @XmlElement(required = true)
    private String description = "";

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    private Status status = Status.NOT_STARTED;

    @Column
    @NotNull
    @CreatedDate
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date created = new Date();

    @Column
    @NotNull
    @CreatedBy
    private String createdBy = "";

    @Column
    @NotNull
    @LastModifiedDate
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date updated = new Date();

    @Column
    @NotNull
    @LastModifiedBy
    private String updatedBy = "";

    @Nullable
    @Column(name = "user_id")
    private String userId;

    public ProjectDTO(@NotNull final String name) {
        this.name = name;
    }

    public ProjectDTO(
            @NotNull final String name,
            @NotNull final String description
    ) {
        this.name = name;
        this.description = description;
    }

    public ProjectDTO(
            @NotNull final String name,
            @NotNull final Status status
    ) {
        this.name = name;
        this.status = status;
    }

    @NotNull
    @Override
    public String toString() {
        return name + " : " + description;
    }

}
